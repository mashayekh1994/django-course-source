from django.contrib import admin
from .models import Article, Category

# admin header change
admin.site.site_header = "پنل ادمین"

@admin.action(description='انتشار مقالات انتخاب شده')
def make_published(modeladmin, request, queryset):
    updated = queryset.update(status='p')
    if updated == 1:
        message_bit = "منتشر شد."
    else:
        message_bit = "منتشر شدند."
    modeladmin.message_user(request, "{} مقاله {}".format(updated, message_bit))

@admin.action(description='پیش نویس مقالات انتخاب شده')
def make_draft(modeladmin, request, queryset):
    updated = queryset.update(status='d')
    if updated == 1:
        message_bit = "پیش نویس شد."
    else:
        message_bit = "پیش نویس شدند."
    modeladmin.message_user(request, "{} مقاله {}".format(updated, message_bit))


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title','parent', 'slug', 'status')
    list_filter = (['status'])
    search_fields = ('title', 'slug')
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Category, CategoryAdmin)


class ArticleAdmin(admin.ModelAdmin):
    # show header table
    list_display = ('title','thumbnail_tag', 'slug', 'jpublish','status', 'category_to_str' )
    # show filtering base on publish and status
    list_filter = ('publish', 'status')
    # show search field
    search_fields = ('title', 'description')
    # Auto create slug base on title
    prepopulated_fields = {'slug': ('title',)}
    # ordering status Ascending and ordering Descending
    ordering = ['-status', 'publish']
    actions = [make_published, make_draft]

    def category_to_str(self, obj):
        return ", ".join([category.title for category in obj.category_published()])
    category_to_str.short_description = "دسته بندی"

admin.site.register(Article, ArticleAdmin)
